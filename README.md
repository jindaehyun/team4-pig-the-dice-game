# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary :
	This repository was created by group 4, to plan and develop prototypes for 'Pig the dice game'

* Version :
	2022-03-31 : v0.0.1-beta-1

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Documentation ###

* Detailed Description for 'Pig the dice game' :
	'Pig' is an old western traditional dice game like Korean 'Yut-nori'.
	In this game, players throw the dice and score according to the rules below.
	1. Players have to keep adding one dice as many times as the number of eyes you roll out.
	1. The player who scores more than 100 points first wins.
	1. Players can roll the dice as many times as they want, and hand it over to the other player whenever.
	1. HOWEVER, If get 1 from the dice, reset the score.

* Environment :
	macOS, gitbucket, gitbash

* Pre-requisite :
	JDK

* Configuration :

* Dependencies :

* Members :
	So-Jin Ahn, Hye-Jin Lee, Seo-Young Lee, Dae-Hyun Jin
